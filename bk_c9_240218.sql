-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: c9
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bcnc_datacard`
--

DROP TABLE IF EXISTS `bcnc_datacard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcnc_datacard` (
  `idnumber` int(11) NOT NULL AUTO_INCREMENT,
  `layout` varchar(30) DEFAULT NULL,
  `tag` int(11) DEFAULT '0',
  `firstname` varchar(50) DEFAULT NULL,
  `surname` varchar(40) DEFAULT NULL,
  `address1` varchar(40) DEFAULT NULL,
  `address2` varchar(40) DEFAULT NULL,
  `residence` varchar(40) DEFAULT NULL,
  `nationality` varchar(40) DEFAULT NULL,
  `gender` varchar(40) DEFAULT NULL,
  `height` varchar(40) DEFAULT NULL,
  `expirydate` datetime DEFAULT NULL,
  `dateofbirth` datetime DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`idnumber`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcnc_datacard`
--

LOCK TABLES `bcnc_datacard` WRITE;
/*!40000 ALTER TABLE `bcnc_datacard` DISABLE KEYS */;
INSERT INTO `bcnc_datacard` VALUES (1,NULL,0,'FirstName','Surname','Address1','Address2','British','Spanish','M','170 CM','2016-12-31 00:00:00','1979-06-30 00:00:00','Mr.',0),(2,NULL,0,'FirstName2','Surname2','Address12','Address22','British','Spanish','M','170 CM','2016-12-31 00:00:00','1979-06-30 00:00:00','Mr.',0),(10,NULL,0,'AMANDA','TESTNAME','145 Sunrise Some Borough ','OX2 7RY','SPAIN',' British','Female','142 cm','0000-00-00 00:00:00','1970-01-01 00:00:00','Miss',184);
/*!40000 ALTER TABLE `bcnc_datacard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bcnc_imagecard`
--

DROP TABLE IF EXISTS `bcnc_imagecard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcnc_imagecard` (
  `id` int(11) NOT NULL,
  `length` int(11) DEFAULT '0',
  `picture` longblob,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcnc_imagecard`
--

LOCK TABLES `bcnc_imagecard` WRITE;
/*!40000 ALTER TABLE `bcnc_imagecard` DISABLE KEYS */;
INSERT INTO `bcnc_imagecard` VALUES (1,9392,'����\0JFIF\0\0\0\0\0\0\0��\0C\0\n\r(\Z1#%(:3=<9387@H\\N@DWE78PmQW_bghg>Mqypdx\\egc��\0C/\Z\Z/cB8Bcccccccccccccccccccccccccccccccccccccccccccccccccc��\0:\"\0��\0\0\0\0\0\0\0\0\0\0\0	\n��\0�\0\0\0}\0!1AQa\"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0	\n��\0�\0\0w\0!1AQaq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0��%)��h������\0Z))i\0QE��cȨ2�=I�c�����+}�A���ӰY�e�����q7�8�k�\0]�d4�4=21��X�H�%�9����\"�ʰa�sN�y2��	���G�Z��`G�;:��).����.znh�q�xո�,��O����L�8cU��d���+��h�?o_�r����oO�G�72�s�9B�f��#k�$��{�9�u���@VY9��7��f�-gë�LG�wH�`��x0a�r*@u�P�E\0QE\0QE\0QE\0��(�n��\0SIN4��(����(��)��T�rI�\n\0Z��<Gof�&����B��j�����c���y_�ָ����Fgb�O,NI5I	�GQ�/�<�vE����5���pr~���Dx�j��?�1���Q��G��j6pE��i	�ظ=;{SZO��P.}i~��n;�8�wU~����rɗ��m$*�\'=���Q`����޵rTH�ф�{}�a�t��p�O^��+��;�NiXjF��#�?*�o����}QȬx%#�<b�Y\n�#�&�L물W:0[��/rC]E����^e����;��W�	T�Pm�j�������������O���[�R�k~��Ġ����lԀR�Q@E%\0�QE\0F�>��W�E\08�iM%1Q@)���9$�@��GmK)ָc�\Z�̊�-�HԞG�^�?��u��Hm���ykp�ֹ�>U9�z��+��1,	�j\"@a��Hr�(\'��i%p��ޙ,B�>G5>i>�	�zR7Jb��j^CH)@�d�\n2{�ҕ�&�Ĥ=i�i6��r�9�B��K�G4\0g$\Z;{�O�\0��zT�ȼ�rx5X�� �2�n���Ҭ��i��*|��J���n*JF�7\r�\"3#)�#�]���E�@�2���\0{ך��1ї����]��{g� ,��==���z}���\"��7fR�:�B{���V�f0��(\0��Z\0�N�RK���E\0Hi����S��I�@���7�\Z�΢\\���A��\"}3�{]&�u�i���H\\}	��כ�7!�v*71\'�q]A��\'�sQg\nv��4��Nx�I���3TM�5-:(�sQ�T����$���\Z�S�����o��g�c*ҜѴ+�X����Q��\Z\0����b8	��Vve�+N8��P�cHӹ��T�9�O�Em%�\\`S��q�V|���[^I`1R=�ې9�j�[.yU��SG8{#������*)l6GN�ԛ�M���\n9��H�8���#>��Mi�ݪZ�-��V�C�c\rԃ�#�c�J�w@�S��fC�Z\'tdՙf=���3�O�)���ZF?/�Ň|��(h~O��Z��h;I�z�NOLT���m��ҋ͍#T���!b��d����zנ��\'�Xw�,�Ȯ��Z��<v���m�=����(�h��dS�\0(��\0�s���O����)��4��J�a2�ZC\"��`�*�s�(־�٭��D���_��;\\\n-֣���l��|�ބt���Y���f1���Y ��h�3�r(Hr�\0\Z6�L��5#��0���i�Dr��2=�O1�Lj{MBĶ	��v ��O�@�\0��\r�>��-�mqp]�Q�겱�O�mi��(��j&�)��#v(�\nF0*ꍣ;k��f)�T���Jc�⤤F>�2�LU#�L�P��UH)h���\0�Y�ی�[-U�@s�c���`��V$��K�ڻ	mՆ\0�N����Z�G=Hu(�)\0�y�Lz� g�*8���<�Rʤ`��x���8��jE$G��Q`��ORF1He��vȄ��R[J��:�择���][���&���8���o��J��#tc���;NG����zf���R�Rԁ�ܤ��E�_�E\0Hh�4�p3L\n��q�B���k�/�d���i�$m�����=��a$a�,�/��\0��󼁓��ա1�dO50x5a�j�g�FN��{�Қw�i���2z\nBWQ�!���2IO�S��4���{Qr\0�~��,{�3�1�}�����&�{}�]#�fi0�f�l�m�X��uRVE�u�W�y^�R�r*��M���5�NH\"�۞�,h�OAN#���mD���x�W�Q�)X��B��a��N2}��1��B���V�\0�1U�i��������$t\'��M�7�p�Ekj��T����	<t�WD]��5f;[�\Z|`���?ʤ�H�\0�@<S�@H�� W*@{UyN1���֧����ʢۖ�pO�Bkh�/�s�9V�z�צ����FEyE��#ҽ#@�˥@I�Q�>���)\ZtQE@�.FY>��Ҋ[�w!�8*(ST�[ű�y�w`�?x��*��\\Ǌ.�;�X�Z\\��j��s,ӻ�ӳ|�g	��G\nJ�?61�ڛt�N��q��9ɪ/30<�؛�$o1���w�$��\Z��«d�z�	B�\'�^�����M6h�`���;T�3���<�֢�Prr�ZG�j��@s����?7*��\Z{�1����I�8��~��&>��X^MB�\r�EL����X�NŢ-B -hm(wST��&�:�[�|\0(��O)\0t� ɨ����:#�M�Nȕ��Pʠ�jĀ�	�qE��T�tTl�;�VU�ԁ��f�\Z�g��$�k��4Y�h�#�G �M�x4�x�Q��&aj��v��gz2���]޲���sWDВ��<�=�\0:��dpj;\03s�f��|�#��Zc���a��#�A���L�R!ߎ��ُ����x����ͦ8\'!%8��y��\'�\rz��2BH��\'��JC��QE�D3�_Ɗ&꾜�@5q�-���ժ\0����ͫ��4(����x�Q�� U�L��N#\'�⩎O�O9�i����u�!���*9Fܽ}�TdsH3@�1�g\'�=�\0�u����P��Q��r9�Z\0V§^O�[\Zj���[�X�%�[����Ңf��+@Z���\0Vlw	��*O�<��ڨ��h�\ZH�5PJ��	�AR�9Fģ֑I��1,d�$�,ht9��o\ZW.ɓ	=N)�g5R\r+�57*�d��Ps�8�I$�\n�K��D7^�H�bT�մa�b��Y7uA튝/<�$�p�CORt-0^�\\�Zw�c�2���)<�ԴRc.�(����c�]L�v0��Z㙛�ZS0�Z��,�>���y=�\Z�Ͷ%���V�;\0�Ҵ0%���;������=��P<��8��I��=��ڎG������	j���+�W\r\0(9�]W��� ���89�*~���)�n)sU�v0�dZ�5\r�a�s�?ҊK����������?ι�tn�ϓ\"=�>���dWp��x��,0[)v�s�U!3��$�?J�i��jܪ� g�s*CO-�#�ȱIcf9?�;%3�g�JFm�M4f��O4�Sq���:d��qҁ����r�z���DvqYZD{��ӊ�\"\\VR�馴2�߸q�v�+-�,d�IHA؀���< ��m��ra��oc��MQfUK�|ؤHЁ*#�z�5Ү�<z|7,�+�ZG̣\'?Jk�\ZA#�_��<S����K+���N)�䨴EnO*�����E\\�;\\ҩc/�\\{��n3ֳ�7�f�P_5\r�\0`T������g%jS-�&��~���Ia��zA��B;����5�w|���A�Okywl0�W���ֵ�s�� ������r�$H�+V�J���m&�F�|���/��[�Aw7�qek+�\0{�9?^j��s\"yD�#]��V%FW1�����U�ԽI���G�V ��ͻ�	Xϖ+}+����:\Z�\'S�\\���q&G�WM��Z���,h<�C�lr���=�qGA�@Ĝ���a|֎�DrA�q�j�C��:ȑ��rOzE&z\r���C*�F���z�}���3,�� �+ϴ[�\0-$�Fe9ލ�:��G�E(�t�X��s���2�+�7\'=����v����z�(��Kr�8�F��	?J�]b�/%�o���E�+��,�&P���o�k�/�Ү ʭ+ʜO]�g�#8�*���po��� l���x��8��zP��.ҏ�G�@\Z�B�(��݄qXZG��g�n��+	nu�ؗg�MjO\"�<mɩ2�EI�$`t��~Uec\'�Ҙe_0��\0� t\nz!cM_����a@Q�P�R&��5��5<R��O��\ZCD��Y��}ޕov�ہ�8����Rv\"Q�\"�]�*�RåG�`�jx؁�)ܛ�x4��\n����T��R�z�1����GWQ5s��e7c��!��o��U�0:�_!�o#۵.э����o ��B����5�q��G8\"�Ȉ�#�o��MW`G��8�(�0p0EL��j�Tg�?����2�H��O�t��\Zyv�gb($c�����SvX`v�ZQ>mc�1�}�Ɛ�w1f� �\'r�Nx�U=K4���gr�l���\"��1|c)G,20x�\ZpNOz�X��Fe ^���+���V�Щ����p	�GQ�ASN�\n�\"�����f��$��G�=��(9<Sz\nq\Zh$��F��~S��؛�s�I��޷`�X�s�������n$g�Q����yb�Ր���.�MT�@��[���8�P=���c�֥���k��4�VT���7I*�)�8�z}\Z�7#2gҮEۢ��(�Iɦ�\'}MheE|�J�\nl���i]Pv��U\0�\\h�q1��p�4$����l��iv���*vLq�j���6�,jK��5��r�qI�=\n���f��Q�O�����8=*�0���7���EV�L��\Z��P4�e5��(�+Z��d�<Y�����r*lsNpz�HX�⛞zWI�H�:��R�������\'ڑNN(\\��q�S��ޑ���O\\��^Ԁ�.�z㚱>J�e��*�`$V������=�\\_�ș�9�nq����p}���3��orpMn>���TT2���A�٘��wm<s\\~��[q�aۿ=+O�%���E���O_ι�B�f~�������N�0:֛6�h�By�#y�=ꑛ\r���.�MO,���I�R���\0�%xJ��sQ��<��I��j�#Q\\�qx�����YOs��ň،sV�*��\Zx|\Z�sH:�z�\n�&\0riL��)X	��\nj����K���Xw&���*ّ�Nj`����ɡUݜsV��TQ�:�*d��4XN���#����P�Q4�l+j#�FǊBri	�Y)�\rr���95�+ak����/��\n�S\n��(��CJ*hUX�~8��Z��!���4��(�ީ<�4�H�,H1 �S^0V@}� 9��N�s��К�1�ps]�bq�J��+n~*��o�i�D��n�ka4��I���A$�����3����0�\0����3���q�QR����-��2P\'|s�q3\r��+��;	�T�@\Z=�;I8 }k�h�7n}{S�2�{\Z�榖��9�7i	��Ҩ���\'����\r��R.���S<�qRE�wfUTRO�4��}�Ōj�Ȭ�*u-�P\"���)�� w,�9����kwK���#y|���I�\\��Y7�B_\\Gm4r\"�T ��zVR:)i�	Ȧ�\"2iA�8��fnVi�2�l\nG�Pq�O#pÎ��譜pz�h%��x�����p�+�����c���N:zn 9����#\r�ml�Z�.���1����J�Qm��?\ZW-�S����J.�p8�/n���c�R�e\Z��5\ZȲe���Ď���s�<RbE�p(�0\Z��+�L����+���r��y��n�J�Dy!yl\n���qq�\\�oeP8�Ԋ�\n��.gds�PH8��	t\\	�WW�xN�qhVxJ���_��s�}��8e��էsXd�$�Np=�F�Ie�aUػ��ry�c���*A����	5b�-\"�\0�EC��3��d��fL�p@��GQi�,��a�ջ����wo\rİ�!�R\0Q�N�ʲ�����ٳ�p��a�����i���<��ˌ�����,�$��D\'bN��:u4U�0x��\0-՝����J��9�j�������\\t�Eu�M4 <��A��_*g��[���8�/�]2�2�\r��+������H�bJ����VP�uy�H�ЂL@,��?J����x�2Rm����*3�r�}O5�Y�f��U���*݇G���aj	��\nǜY�Os&,�����_�\n�tM\r)���)eT%�����\0[���� �k��u�cLn�+����QCoj����#s�]��Վ����RN\Z�O}��D��W�S�X�\\�9��#V{W��\\�ӒI�6��i�Uh��=*@��Zdꔯ#qH�T��@�d�3\0:M8J�g\'>������8�s��6���n��3t��WŅ����D��#h��e��q��A�^0\"�1U�\\\Z��\n�����pB��@��{�{ˏ&\"��>�0:�U봲��N(����3�w9�>��iZ�v�[^�w\"f)�_���\0�?�r��q�o3C0x��L�~��-��}O[$�E\":4nF�!�\\�Z~���nO;����U[�P�(�I�[9t�b\">�zVpW���vVn��V�yfOL\n���<V�Z\\��&�Lh熑J~Y��S����r��\"���3�Z�e��70:}+9\"hܮ0��Ήz�3�H����I�v��C�d@�d�H��+pSbM�&*\n\"��_���\'QE $jm9���mai�%H�^����V����1%��\n���5��3�n�i�3\npZT\'y�<\n��=R�Q�3\\�Y�(?*�j�6����!d��iH�����z��ڭ��&?�P<����ސUYvJ��$�ԓ�jy�sH:�S��t�\n�z��wH|Nɜdf�[OsY�ޔ��Jy���P0h��N��.zsR�/�@�0iIS�M\"��(�S���٬��)�ܐ��5c]�� ��H\\�9��:\ZE�\ZF~1��Ix��;硤�Y��x�NH���\0S6�L2���2}��%�[��zUK��g5�XՓ�\\��ɫ���z#$��_�5���V�#6�7j�����BP�y�*��]�h���<��|�T\n���W���rY^�s߉�u��+�t�r�ULA&ـ��r�|w�H�d��u���K�� q��`z���6�%��J�B]�R��9%Ok`Xڇ.��3u)��늜\"��\"�,�ѣ�h�#�f�*��익@T+�#����\\h:u���%�N�����C)�(��@����֖�ZC�E����E 2u�A����ig+�h�(�I�+��|C{�F\"��c�v\"�>j���\Z�\0?�>��s1,=�H��ޚ��4�TH��=)I�SW qK��}oJL�x�(Ԗ��W\nݳ�D�R��qvg_��D��Ҩ�7\"H��n^\rk���b�SBT��@K/Zڒ%`r*��t�14ў�{ԋ(�8���9�6��H��7hy�(��WkY�S��\\r\rC�&i���4�#��1�t��-� H˞j���R�n���j1Z)\0���\Z[��Ն�w�����;��3�rM>{��B�6X��э�j��a���L��\Z�&8\Z�.	X���y��*8�n���E�nMJ�i�x?�����+�VX$Y#n�� ׌+`�kOKծ��Ķ��?ĭ����zԸ�)K��9��\r����,2b���\0���\'�V�5(}%&h�\05���\Z)�����w�1�l�A����]�z�z�� ������a�+�|ֱ�͉�M=�@�a�0�8��ց�!\'8\'�i/NE\06N\r3�=���)Z7�a�q[z�\0Y��-a\n\\Դ��f�vV���?w\"�����\\B1^U��Q�[�R����H��t����ՔSI��<�����~Z�u���ԹJq6J`sMs��> ����j�MvR~H�_���G#<Q�#^�5�������r��w3d4��Ub�{��4�>亩ll���H@{����Y�bI=�G��f�I#M�I� �f�y�!�tQM&�Gt�A�SI�	�\0h9�)9�LR�6N��t\Z_�/��X��u�!;�ѿƹ�z1�!�c��r�UV�w$���|g�1�V�j�m>�k)�x��x=��>��}��_2�p�	�\0��c��I��:��� �}�����n[�L0�qߖ?ֹw5����F\'��������e���c�q�����9�1	KI�QȠB\nw�G�Cb�O4��sHcz�^�c�1@�8�&��8���CI�@��qA4��Ҟ:P!��Ґ�?\Z���4�LB���\nj�)�	K�CG��4\Z^�h����M=ih�i��j jBZ@K�~�4o��⫩����S�x6�{Ğ�z�����z��QT���_��3������:����?�s\"�o�\0	��\0�)��5̵h�%��C\0\n���sG+@��� �	=sL�v�)A��GS�t�rqHkA�t�ci�ҀCN�GJP\0�ǥ\04ъ^��`zPc�\0��t��@��\ZRH��\0� 4��=\0�\09qޗ�9��qL��qIJ�h�4\0��h�APh�AQۥsOtsLZz�P2NÃN^��N1�S� qHGc���������j)>����-3��c��Y��[<t���y��k�qһ/�@iY�ra`O�\\{���З��c�\rצ(�Ҩ�>Zi9�J�z�zPRK�h�ݠ.\nBi̘|)��(�@�)�O��@�S� ���)E ��H����A�����F2OJkd�ӻ\nCҀ��~t�\0^*\n������\0�A���\0�Cv�Z�\0�c�R7Jz\r!�����%8��>Zp�rv�g��>n���_���}����բ�kSE���',1,NULL),(2,7783,'����\0JFIF\0\0\0\0\0\0\0��\0C\0\n\r(\Z1#%(:3=<9387@H\\N@DWE78PmQW_bghg>Mqypdx\\egc��\0C/\Z\Z/cB8Bcccccccccccccccccccccccccccccccccccccccccccccccccc��\0:\r\"\0��\0\0\0\0\0\0\0\0\0\0\0	\n��\0�\0\0\0}\0!1AQa\"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0	\n��\0�\0\0w\0!1AQaq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�h��\0(��\0(��\0(��\0(��\0(��\0(��*�����b2���~t�U�/m���̪}3��\\��q.v�Uq�2?ZŞyebK���W?�`L�q�f�f���$?qbPO��l+6E!F�0)\\�S�_N�\"#�O�ԩ�w����k��~�JiO4\\V;�_Zρ h��V�r�*�Շ��כ��� �+B�T���3�1X��+&�[�`�i��������4uRP�E\0QE\0QE\0Rb���IKI@Q@Q@Q@U;�F����@y4m�QIb\0��e^kqDLv��_��w��^˷qU�z������i���Cc��ux��ݽ\0<¢k[�1ڵY�%�\0��M9c�Ke�\\��gdP81Z$�FH�E�9QT[\0([u��C�	�R1Lk瞵dS���lN84%��z\nr�)�K�R+iQ����5�iysl��A�MFf7Bp�V����kK���8a�j�a=�Il�[9�W-�7�w�7f�j���E >�P!h����(���ZC@EPEPEPE%Es:[@���G�@�ME,b��V(���B�I\'��1v\'\'5f�i.�i�8&�co�28�e$X����D��y�U��.z�37\'��Q��E�TI]�*�w��Q�\"�)�f�8��)P.\0�QO�!��4����.1@	�(&�= �LD���B�P\"�\0\'�}�3!x��OZ��A��\'�Z&e(؏L՞%����k}]!H�\"�+�?|$S�ǚ���	<�s哌�\Z�܆����R�H��h���#4Q@EPEPEPXZ�Ǚ*ڣt���\0?η	\0z\n�2n.$��ķ^�1�1�ø�1��Gb�wjGo�T3X�2N�z\nf�	�4��I����P1BѴb��AR�֜��.!���(\"��g�&�\Z`7ڐi�s҂��LC@�dR��\Z\0PqS�����8�LM\\�:����s08�Z�C��Csh򾽪�2h��.��}��t��+B�]:��^\"�|�J��شRR� �4����(��(��)(��0��W�8�>����;�w`������`8g\0⹻f,=�&TK�}{Tst��a7�ZS�桛Da4\nC�(椱sJ�n1F=\r\0H$4��T#\"�I��1�Rs�)��wJ\0]���&r)\0�	���&oJf�LD���SP\r0�h����P�?lԱ�<���ȩ���~��!�6�Y����_̶���*2k���2�px���)�XFl��b˔RR��QE\0-Q@Q@%-%\0c���m�cX6*C�<�Ҷ|O��g?7���-CJ��K.%���b,}�_e��SP�Z����8╊L�}q��X��eI�JI��l\n�T�Y�=ɩ!�������;tvڑo.	�G�ִ�O��M5���]\n�m�H%��gY�Y�[����uh�ZZ�l ȥ �0K��V�zE	�\"��1e��,�2E�4��G�����������R*�;�1��D�#�[�����<H��\n�r�ar?Zz��?6�z9E�t�zRb�`��|��Mk��Gr����Z,>bh�oJ�n���\Z�\Z�E>^A�\"��Kd�db��f\re����so\'͌~5�h��\0 c,MZ1e�ZJ)�QE\0-Q@Q@	E-W����kc>��_����\'�a���+GPX�+oV��\ZYa�]S��=1���eh�D$���\"��H�2����Y��WR�j�$�95}\r!�$j���4n��\0V]��ݍ�4I�~�U��PLqK8_���)\Z;#�,����\n�+�mH*��d�\\��q�n+ḡ��pVA��m0RE�_J`$hIKp�}E<!<⤡��ԛ�����ةLG���[^YOB*�!�W�3�E1��=:ԩ�NE\0�S��Ƽ��.Õ	az��Z�H**�F;7����G͹~�E4\'V������!Y�+(��Z\"t�e�hxĊ2(�.&�j$�8�\Z���[����c\'�Ү�3Uc6�9���1�+��Wn��\'�rm��P�����?&���R&DԴ�S$(��\0Z(��\n(���=~�Xu��wD�0=8����fM]Tz�S-�%y�OHm�*�)WB2}?�liq�l��Fj�D\r�����e��01Sq�$�;�,�՛*<V���U�sR�4[7:h��$��;�d���F�*\n�WD{��5��CB��NZ[��i<��]�m���������;����Ll��?�9mB�\'��\'ٙʭo!1	Y����Z�\Z?J%@\0UP�NK��L���⭆ �����T�b)��F�~�$�2���U�ҨM\r�x��ܞ������9��*�d����M��w>I�sR!��D�G�pG�i���1n.��L�L�FF�!�q�>�E}~��3���ξ�=݇ڦiK��r�F>�.�n�<���w��ګC?yEq�1�a�Xc��is�.a��	�\Z�c4�����\r9�sR��j�6$ȭ��s�ڲ��a[&��*�g=��>ծu �O����4F���[��2��60H5�$��?��\0y�\'�\"��k��pʳDz\Z��!IKI@EPEP\\����drxA�+��WWLjw��?*��kKr���e�Ú޹o��\\���H�\nܸ��\nQت��p~lT*���V�!�Jt}jY��:x]�<j�R:���\\�␡58AHN�$`\n�D�֭?$�fm텠�9\0��28�KҐ�%<t���W]w�5\\|���F�*M�`�X�\rNTL���Bzc�M��8����T��c!�WJf�ъb#�r+n,5�=�Y��3Z�GW)��F,3��Eyq\"�ƿ.�\0�jm�s\"�эS�R�p�\Z��͢�;\n?+I�}��\n�b�;(T����Z-�W��\ZZJb�(��(����Vt�GU�tUOR��s��92Zfs�1�rd#i#�\\��j�j�gu�A���N�l�Ғ���E���,�[oz��J@2�5��n}i�g������( t�$݂j~��n*�h2L�gR�i���ϳ�H�Ұ�\"���1���;\\���ۢ#�S6�qXz~���X����W��}iX�h�*���ySYw���zL��G&�[��9%U��ӱ7\"%~V�*ʶF3Ue��:t�D��?\0�h�)7`u�@�\0�d�cC�ǭD^�\'��W������	��&S9�I\nj.���8��}���1��.�2߻������<��.>X���f��\'MҖ�+S�(����(��(�����l�:��ME\0��R5+x9���/Enj��m��8,8Ȭ;�\n�Y\'wr�B���֘H�qK����P�۞�2ɁU����P2�_�)��)�����y�N2*�����ϩ�3���4��=��4L�-}�\0��UP=)����z;�`�x�Y��o-FH�bT�m2�S�ӆ��e�$$���H�*�\'\'=kB�E.^G��b�N\0���0:TQ�1S��Y pF��a���})q9��Rg֐�\0ru�b1Y���h�	��\\L�R���nh�=H���H��J���5N��-�S�l��_Zڦ�R%-,QEQIKHhh��\0(��\0(��\0��)6�?�s��]U�^}��gԌ��ɗ�>�24�Qc�Ҕ7��ޡ$��Y�c�z*��\'�~p��q�砦\"����֣�����S�7&��V\"�k(G��ݛ�kE`���������8ǵ�.V�R�\08�����{�ڈ$�B��Ry0G���)�!��T��jHex��Z����r��wb�1�}(��Z\'��0�V�ɬ83,H\'��K���y44���Znp)����\0�(S��Y�:�&�,B2®�v�lU;u;�[�j��^ʤգ)2M!.Z ӻ����i�EZ2aEPIKI�P�EPEPEP\\~�>�y\"��鎵�W7�g�w)������&���1ك���]�Z�\'xx��\\T��$��gc{ܙAM���3�7pL��\n�qpd$��ڋ\n�;���a5TH�S\n1����d]�ɫd��6	��j�@f?JH�ܞap=qZKj�����{P;���F��~�RI���a)�`���?*C�3R�w����ꏑ�U��Lr����[M���S�6�.��\07�Sȸ;�9t���G�sQ=�@䴅��B����\Z��l\\��8�h��4��c� Ӑc���4G$P�q�\rm�ɻ͛�p���\0�����]]�f��>�d�MZ2�,QEDQ@RP�E\0QE\0QE\0T��wk$-�C��{\Z��\0��N���>�*�H�G�v���v�/V��A����r?6�1Ҥ��5܎�Kc?ʢI7��l�Gy��ME�n�B(�vvx��f����6��\n�\n�T4m?����OK�yMWu8���:��!�i�����E�=�\\��7\0\r��1g���4�.dvi���|���s���t�N84�Ջ-\'Vl����\0�VKu�WR�g��4L��m�|�2�n���r�*	7E߱�$�6Bn1ު�w4��䑊Atڽ}{VA�y{U�EiTw8iX|�K��o�]�Yȹ\'�{\n�*��f,,���%��M[�FM�QL�(������(��(��)(i(�����}���@�:z��s@�& �C�q\\5�f3�>Z�u;�z��yRD	 ���)��R�Q}Myt0\ZBH�\0v�e9硫��ؐ8>�R@�y�$2m �ں&\"�s\\�v��֎�s���*.����2H�$r�q��jrȣ$�X��2e�N���G��\n+Xθ�1Bʽ3@��IjA�T�Hp:�L��(�\\�/˚½vwn���Z��1�;H��kC���ҭ+ɑ�N�Gn�h���h�3Ҧ*K��$� �%a��~5�xb؋đ�0x�z��e�s�u�/�S� ��SKIKTHQIK@Q@%-%\0-Q@�����O��(�AǗ��Ҁ5j��[L���\'�g\0�¸mG��Ӗm�����~\'�+&&F����o�bI��;��^#��2����i�Z�&���if@Hs�����X&��-�k[Lu]�5�$�#���P�CJ���$y���FN+)��\0�V��7_J�Wi�k΄�W�����=�,�p�/�tR�9\r��j��dR�U&L�s�m\0��ɴc�8�W6��A��FNW��3�҂��n�)�{ � ��I� �Jge-�����_mE���Ԑ_������]d��)#=��E�����6E-�3c9Ȭ���622>�X,\\J��q�D�1��@�o#&�[[@[��L�U�d`g��6�� �Xm��1W#���c\"�e(��\0\0w�+f0�+�U����>1�b���g?�ro�R���hk�o^K�u�wP0	=k��x֟t�\0T�G�mA�L����I�F;[؎����z]-s�_����\r�8lt��ʁ�uu=\n��@:�(��(�!����M�3�C���~��sZ���w&�H�$�??�+���y]�YG=Y�����$�f�\Z�����a5��?h��c=�w�~�>W��1�9>��Y:�����F�P��ke����b`��lh��4\'��c�8���p+GF�m��Q��l\\7:�=>��N��>��:QU�w����a���ҕ�F1E��)��Kd�UR{ue!�Ej�*���*��T�&�nFP��g2��Bʽ�y!��U�G!��\r Gk����1MK$�J|��f\Z[M\'\0`{��i���\'��n�p1HgLb��)N�����5r8\0nG$C=*�qg�u�\Z��0{#q��L��\\���X��G�j[�	a9?�Ϳ�=F*���t��t�8�)-J�d\Z�>����l�[��K֗rC)*��\Zܴ�\'�\r��\0e�?�r����T�6\0��p=\n�Ų���T��|��t?�tVZ����o2��J~�q]��kF�Q��*A��Ȣ�=Z���i�%������>�~=�\0κ[]R��=�N�U�R>����Id�ʪ��ea�\Z�@��sH��N<q�ҚJ����G9�H��M��J7��=A����?��\0§�%��E�n�3�Le�;b����&5�;8�r���qYZUК�s�/V�����#�E&=)���)H�N��!d�\']�#�] �P��t�14Wڧ�b���zs�9����$ps��Qy�O4�~i�Aʤ���K��\n��a@X=��(�/w|T��T�RCH�9���S�)N��u��X:g�!@�I[h�OZ��E���#�>���2��(t\0v���۞�t\\�Z�22i�08�u�)��R��ң�\Zz)9��@�^��x���jEy��Es���f9/�br2Gz�{���ni��ޤcp�F�-��T���g�Lo<��x9�h`3�G�ڕ\0\'p��ix�\'�ր�u�Hp����\r��i��\n@Mexֳ+T��ں�i�X�!���[5r���m;��_�nk	�C�-���ޕF��;��F��zU��+�o{��4�S�h=(���DS��VK�)F8�0h�c9��A�*�L�p��N�ت����\0\\1�*�M��.(��\0\0���_\n)���R���d�$Xz��@1����}*��-�I�j{��C�#�k�c�(�ٹ�9�<�@\0�����)\\i}(?�8�i@ڴ��iTS�ϥ �}�F)䓎zTdR �s@�\r�����ޙ�*2jt���#oS��a1Y-e�3��\n�|���������\0.�_���\0½��B��\'N��ku�\0~�)���<Y\\�\0߇�\0\n��(�0t���������>�u�\0~�+٩h�.4۱������\0�8i׃�\\�����f�����L���;����\0�I��x�,�����\0�{M������j�п?�lAqs�	��������z]-K�e�mD���\\�3�?�&��2�\0���Z�����K�K�\0<e�\0�\rD�� �\0���\0�\rwtR����p��p:�/��4���\0�c������E�ھ��i�猿���\0\nr�ʿ��_���iE���G\Z�:�b���5B������S��,G��Kڮ0�.��y\r����[k���X_��R{�x���\0�-��tV�97��cM�����\0�-��b��>�qǬM��tv�W<HY�����2o�ky�7A7<�ҽ����gv���O��ԞS/�G�B+�(�.xo�I�W���S�=�\n�)5�P<^��d�26q�\"c�+I�����l��YD�Vl���\0�z�y��@:��恣��',1,NULL),(2,6530,'����\0JFIF\0\0\0\0\0\0\0��\0C\0\n\r(\Z1#%(:3=<9387@H\\N@DWE78PmQW_bghg>Mqypdx\\egc��\0C/\Z\Z/cB8Bcccccccccccccccccccccccccccccccccccccccccccccccccc��\0L\"\0��\0\0\0\0\0\0\0\0\0\0\0	\n��\0�\0\0\0}\0!1AQa\"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0	\n��\0�\0\0w\0!1AQaq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�\n(��\n(��\n(��\n(��\n(��\n(��������S=��\n\0�K\\��B�io��H��\0\Zʛ�\Z����*��n?�+��wEy��/��w\'#�eo��sy�V�q��F֋��G��4�5>�O!�;��[6,��%�F=y_Ǹ��c����h��I�\"��\"�� ��(\0��(\0��(\0��(\0��(\0��(78j)$?5-Q@Q@Q@Q@	A!A$�sL�h��iep��$��㵝vk���Z(�}}>���\Z:ǉ�\r��m��Sʏ���J��iܼ�3��&94�R�8�G5E�6��B����CWbaSq؎+l��X�Z�\n�8�0�J[<��Q�=;��[;��SځXͳ����u��3�uS����WEv	{6p���\0\n�hМ��F�!`SR�-s:f�%���!·V_����9T���T�U�r�E�(��\0(��\0(��\0(��\0����}�(j(��\n(��\n(���4��K+E$��\0d�+�����&&ʹg���=)=\0f��ɨI�V\0~D����f�y4��Y�6�Hw�)w�qU��T\\�}�,o�WV�آ�.��%9�BR)�&M.���R	j�\"���ņ���]Ȩ��L�@x�Zf��t�9h�˟��e���RAZiؖ��d@����A\r:�_j�D��f�ԇ�xS���UZ�0��)�QE\0QE\0QE�Pw��E\0OEPEPE�!T��4��H�[H��f��{�uǆ�u�Z�齿�|����콪�<�m��>p*aC�@Z��R�Ӂ��E�K)�D�Rb�VNZLS�1ؕiwv��)�\Z\Z���51�ۭ2\0T��b�朧��� �A�>������|r1S*��ه��p����\n]虜#e�>��\0���H�9#���*�\n(��\n(��\n(��\"�|�R�ޢ�$��(\0��(\0���}\Zv^�c�}�Ҭ/HK�7`���&��6V�Ҙ����Y3D5ڙC��0̉ԊLcʚxR>�T�/aSEr�j��E��sRT+*��Ӄ����M�b���\0�Bv��6ժ�a��,�OZΖ��5\\�1cO���b�H1ڳI��X��u��/EY���/��c�q��t?�S��S�܄�i�����2:R�-\"v�ҭ������zU���(��\0(��\0(��\0���C���$��(\0��(\0�W�r6�/g�h����\'� \r埨��\0�Җ�G4FZ�8Zv0I�����e9݋|��99�a3R$`v�{�sھ2�=)��\'U5��\nk �JW*�D�#�jrG�\"�4�46	�o��*$8�h49ʚ���W�@ˊ	eW����l3�*P*A,i�`���@��=)D��\0x\nR��S�@Ъ��Vb=���*doJh����so�d��2��ߟ�[u��DmK�\0�:�}��+����LZ(����(��(����h�~�h�	(��\0(��\0+��{����\'�+��C�HV��Lp����\0^��h�߅�I�,�	�f��CL�q��AQ�.�㊭{�|�6�=Y��J�E�9;@�Tb��\'�Ӛ�q��t�G�@�����ԺTKq3܈� 3��0}��4b�R��8���`���a�lF�֤�BP�R�b���1O�*��zT�NE\"���Ui53�*�4	���,�\\j[�`\nl��As�Jb�����K�)A��⣸Y.YL�I&с��*�����i�1��2p6����W(۠�v<\n���Rb,���S��$}ϗ>�sC�jM���z�^8���`zU�8����ϚՐ0��\0^�j�4[�e|�2��@�?�WE�#i�$P���Em��Z�TRR�QE\0QE\0A)��Sg��,�E\0R\0��(�/�ǧܺ�X��ʀ3��\r�97e��#���&�c�D�G/���+�I����Y�\Zr�Y1���*W�����R!u\rQT�8�U�Qp 1�� ��c�iQ��E��$`t�TqN�b�$�ԅ���Q��Sr��VbBjӑ��	U���U�y�\n��Ut� \n1Hc�J��b�\n)\n�u@\rL�O��(�,*�)��SN4�¯b҃��J�j&\\Z�+�a1���f��\Z��⧪:Αo�������\nd�,hY�h���P\r\0G{�b��?ץKE^�׵�sgڊ\0��(�������Tu10�*�=\r\0y��nG�C8^j�h��o��=׷�~VT�$�;:7D\rҚ�N�����@��FFi��-ABl�+�-.�S�\n`d�* ���n`;RIq\r���\0}(��*#��ǨA#\0�v+��*�X�H��)��P�q՝�\Z͗UÅ�o4���$@�:�F���[{���ʘ��	�Ͻ&�(�\'Hb�)۩��s@�*�H���J�52�F:���*�Gu��M\"��ϟ�WiB\"��`S�s\0��(\0��(	���j*;��\'Q@袊\0(��\0��.�/,L���.?�w�I/�<^�y5��́��$�n\"9x���O���VsF���3Uç���\Z쑗�摇5�:�zR楔����)��+dSE\\B���cvb�`�i��TҚ�V\"6��3}*�K��zU`�l��s֛��IR�14 ƃw�)��)�rsK`�$[݁n�m](4�\\��\0�d�I��C�>�);�j��Mɣ��ڛ�V���?��+:!��+�瓲�^���\0�V�%�t�QEjdQE\0QE\0S�Pe��f\\��Q@�E\0QE\0���{_?N�R\"�D��HS���V���#�p��S$���P�ͽԍ$2��7%T��W)�ٵ�ܖ�w#��7dg5��h��x���j@ώ*2sCi����[�Tn���r�f��s@\\�b�L��4��J�s@�� �i�WS�9�˨�sE�xR��	�(�M��R�Cf�\rAg��(�J��2�S�2i�\Z�F������5@1�һ;+8l`@��\'�SY�m���p���\0���kdشQEP��(��(�߭�2h��E\0QE\0QE\0�����\"����\0����UMR�^��AܮW�9��h�W��.�f�\n����ɬ��Q�N�E<�S��1�Տ�w�V�oPi�_|R��}�UL�����Fផ|+�Kw�R�W�lR}�8f8��BX��z�3NeQ�F�;�m�9��A�*��)�w\Zؐ��85\0<S��$Z�\0�Ud�߇��רČ>E�ێ��\0�M!3���ͮ�Da�徧�V袵2�(��(����:���4�<��X�� $�Njz\0(��\0Z(��\n(��\n(��\nJZ(�u�Z��``,�C��u�����#�c�y���T�\0��λZ�h�=\n�H)Z�)q�*&�S����d[�i<�wc�J=��sLdBy9�&����/�3E���皕M+&\r0���HZ�*ōL���16Y�f��Y�};�<��?@8��l����^��@m��x[�$j��D�h���B�(��(��(����\0(��\0(��\0(��\0(��\0��]�c�e�l�������J7��#�JT�@H=���Z�˨�$���jZ)3!�BI��8�0>�6(RG��*��O�EH�\'\0T��U��@��\n-�R��7���&�e�5`sڙ�\"�x�K`����<�GҶ��9���/�o_aM\"YB��ne_�!��#��+g�ҳ!EH�B��\0;\n�gѫD�E�TQE\0QE\0QE\0QE\0QE\0QE\0QE\0QE\0QE\0e^���j��+�Pj��>��\0Z�ERB9]KKhwI\0%:��>�����KaȬ-KGIw< $�Ã��q�Ԏ`�;SIn�bd�	r�V����X�ڠ���9�m��N~9���i�����w�)��E��Ic�N���\\/J�Ҵ�o�`V.û��d�U�޸8+��������%D]�\0���$HT\0;���V�c6�/�Z�;\rVAަ^*�]�U!1G��Y�@�\0ZV(��(�EPEPQA8���(��(��)��FI�\0-]\"�5Z[�ہN¹��|?֡aAr�19&��!��x�\n��oN(.�N��2� \"���&{BZ0e�{|��k�q��4@��q\Zv<��T�5�^�6��\'���\Z���Ǘ0(G�?�f�F�H�1�⍙�ƅ���d�@4B�Y��Ҵ�,����v��O��I��?I�	u��r��]U�\n�0\0�F01�=EYQ�Z(؆�*�98��8�!��<\Z`�()�J�8���N���}d���\rVSO���*0�J)X.>��\ZvE!�z�����آ�$���\\c���b{Ӱ�Zi�:��Q���U$�D�E��nY�\nq���bz�f�kt�!�Uk��jE$\ZY\0u��a�\0V�Jq⣃+�EK�\0B)�)��T��6�?��I@�\\�\0����-�{�?J�5/I))nL1��,~��Pԯ..|ƕ�ٛ����Yb�c�M�h�6�p�=�=�s{1ne�?�V!\'�3�xA����$f�c�<�*Ƈ�,��l�S�֤�T���I08�cU��[85fKu�7(��N�W5쵹��e�\'#�w��/�τ�g��I�O��	9APIxx��G�t�p�=���\'rY١���\Z�V$�1h�nz�c�U�o��2�g���M��4�b0�AS���m�����O(U<S��Y�S@=*:j��\Z@86)�*J��@\\9w�EGrA���$���38&�\"���4�S�3C}�\0�8��)XS	��Rn��u�\nb��&j�\'��>W��)���ƛc���Vb�$���T�nn����*�����2���?�zn�\0���YC�á�#���H���p7��%�(���bZs�2V�N�\'�\Z͸��	b�洘�M��v5/���1�S�U�V�2;�)����z�������ƙ��\0��H���i��@��I^�v��o�s�ڨ��0\Z�-��(�Q�����,�|��AW�\n��M�3i�I�X��^F*�[�Ќ���\0d��(\'Y�#��՚#\"����0�����jd�TU}��x��qH�ȩ�#�R)�	3L~\r.h<�@74������/�E%�ĝ�QH���wjcq@CJzS�O&�\re�u��1�4Ȧ;db�+Q:S�\\\ZY[{�Ts�4�R!��h�b�0�I��Ґpi�\nl��Ȧ$������qux�� ʞ��5�$���g��B����A� � h�>j��$H��`Miơ@�S�Ɏ�	��r#�uj�9�_s\"����4���\n;T�a$w��t�\r=Tf��IZ]=rI����h/\rHd�)Bi�EH4\0�COX�Êr�\0�qON���/Z\0z�ӊjӇZ\0q�OjCH\r �)���0�P[�<ν���#��zQ@v�ߊ��PjqRu��0%���8R�\0�S\n�Ը���\0@��h���Td|�TnZP�Ղ��1��F\0�R�v�V��\"�H>#L�W1���\\�l�E4�f�;I��ָۙX��	72I���/l��̱���ql����F�O�J��[I���=�\n�#�)��%�~d3�Y�r��hxy�[�w?0���u�z�堹k{�x�*?j�7,�G,g1��S�\rjA2�T�zT+R����5��@�E\0sH�)�\0�Kސu�<P�\r/jk(���Q�\Z�.����QN�o��R%R⒘���1�\rMQ��CJz�V�ޘ���i����Q��Sb�#��f9�)��\'�U�W$NO�@^ �6�V��#�q�\03\\��b	Y��NߥmjC�ծ�~Jɱ}��\",�n�vA�}ƈ��]�M ��km\\����\0\0\0��b�I �df�KB���P��/�ڽɭ��9,��S��+���j;�b��~�<�=A��tAg�n�+����0�Nj/�X��RT��Q(���ݦH��4T�8�	PqJh^)[�\0;��P\0:R����\00Q�;RR	���ڊe�&����',2,NULL),(10,1,NULL,1,'http://www.theidc.org.uk/theidc-members/assets/ph/theidc_showphoto.php?uis=4d63773fa2536873b3680cee904461b6&pp=1'),(10,1,NULL,2,'http://www.theidc.org.uk/theidc-members/assets/ph/theidc_show_signature_photo.php?uis=184');
/*!40000 ALTER TABLE `bcnc_imagecard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bcnc_status`
--

DROP TABLE IF EXISTS `bcnc_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcnc_status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `idnumber` int(11) NOT NULL,
  `userid` int(11) DEFAULT '0',
  `statusdate` datetime DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `reason` varchar(250) NOT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcnc_status`
--

LOCK TABLES `bcnc_status` WRITE;
/*!40000 ALTER TABLE `bcnc_status` DISABLE KEYS */;
INSERT INTO `bcnc_status` VALUES (15,10,0,'2016-07-12 10:53:42','Pending','abc'),(14,10,184,'2016-07-11 14:55:19','Applied','Form Request'),(16,10,0,'2016-07-12 11:58:51','Approval','asdfsdf'),(17,10,0,'2016-07-12 13:57:20','Applied','123'),(18,10,0,'2016-07-12 14:15:43','Approval','321'),(19,10,0,'2016-07-12 14:19:37','Applied','go to applied again'),(20,10,0,'2016-07-12 15:33:38','Approval','approval 15.33'),(21,10,0,'2016-07-12 15:39:17','Applied','15.39'),(22,10,0,'2016-07-12 15:43:34','Approval','15.43'),(23,10,0,'2016-07-12 15:48:45','Applied','15.48\r\n');
/*!40000 ALTER TABLE `bcnc_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_church`
--

DROP TABLE IF EXISTS `tbl_church`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_church` (
  `idchurch` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registerdate` datetime NOT NULL,
  PRIMARY KEY (`idchurch`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_church`
--

LOCK TABLES `tbl_church` WRITE;
/*!40000 ALTER TABLE `tbl_church` DISABLE KEYS */;
INSERT INTO `tbl_church` VALUES (1,'Church Test','Test','www.churchtest.com','test@churchtest.com','Address test church','0000-00-00 00:00:00'),(2,'abc2','abc2','abc2','abc2','abc2','0000-00-00 00:00:00'),(3,'abc3','abc3','abc3','abc3','abc3','0000-00-00 00:00:00'),(4,'abc4','abc4','abc4','abc4','abc4','0000-00-00 00:00:00'),(5,'abc','abc','abc','abc','abc','0000-00-00 00:00:00'),(6,'bca','bca','bca','bca','bca','0000-00-00 00:00:00'),(7,'bc','bc','bc','bc','bc','0000-00-00 00:00:00'),(8,'a','a','a','a','a','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tbl_church` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_program`
--

DROP TABLE IF EXISTS `tbl_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_program` (
  `idprogram` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `totalna` int(11) NOT NULL,
  `menna` int(11) NOT NULL,
  `womenna` int(11) NOT NULL,
  `childna` int(11) NOT NULL,
  `typeservice` int(11) NOT NULL,
  `dateservice` datetime NOT NULL,
  `timeservice` int(11) NOT NULL,
  `idchurch` int(11) NOT NULL,
  PRIMARY KEY (`idprogram`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_program`
--

LOCK TABLES `tbl_program` WRITE;
/*!40000 ALTER TABLE `tbl_program` DISABLE KEYS */;
INSERT INTO `tbl_program` VALUES (1,'Program1',200,40,40,20,1,'0000-00-00 00:00:00',0,0),(2,'Program2',100,40,40,20,1,'2016-09-28 14:34:09',2016,0),(3,'program3',100,60,30,10,1,'2016-10-01 00:00:00',0,0);
/*!40000 ALTER TABLE `tbl_program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `useractivation` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `userstatus` int(11) NOT NULL,
  `idchurch` int(11) NOT NULL,
  `idrole` int(11) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES (1,'admin@rsantiestevan.com','075edd1338f6a2f8fc3204d54d894c42','Admin','','admin@rsantiestevan.com','','2016-10-01 00:00:00','',0,0,0),(2,'demo@rsantiestevan.com','21232f297a57a5a743894a0e4a801fc3','Demo','','demo@rsantiestevan.com','','2016-10-02 00:00:00','',1,0,0);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-24 16:04:00
